package encryption.demo.controller;

import encryption.demo.service.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class EncryptionController {
    @Autowired
    ServiceImpl service;


    @GetMapping("/greeting")
    public String greeting(String name) {
        return "hello world";
    }
    @PostMapping("/uploadFile")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file,
    @RequestParam("filePath") String filePath) {
        try {
            service.encryptFile(file, filePath);
        } catch(IOException ioException) {
            return new ResponseEntity<String>("unable to encrypt file", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("successfully uploaded the file", HttpStatus.OK);
    }
}
