package encryption.demo.util;

import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {
    public static void writeToFile(String contents, String fileName) throws IOException {
        System.out.println("Writing contents too: " + fileName);
        FileWriter myWriter = new FileWriter(fileName);
        myWriter.write(contents);
        myWriter.close();
    }
}
