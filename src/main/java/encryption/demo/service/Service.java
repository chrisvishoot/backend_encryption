package encryption.demo.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface Service {
    public void encryptFile(MultipartFile file, String filePath) throws IOException;
}
