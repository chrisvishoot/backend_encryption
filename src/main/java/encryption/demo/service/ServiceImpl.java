package encryption.demo.service;

import encryption.demo.encrypt.FileEncryptor;
import org.apache.tomcat.util.http.fileupload.impl.FileUploadIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class ServiceImpl implements Service {
    @Autowired
    FileEncryptor fileEncryptor;
    @Override
    public void encryptFile(MultipartFile fileToEncrypt, String filePath) throws IOException {
        if(fileToEncrypt == null || fileToEncrypt.isEmpty()) {
            throw new IOException("Invalid File");
        }

        fileEncryptor.encryptFileContents(fileToEncrypt, filePath);



    }
}
