package encryption.demo;

import encryption.demo.encrypt.FileEncryptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
//        FileEncryptor fileEncryptor = new FileEncryptor();
//        final String secretKey = fileEncryptor.generateKey();
//
//
//        String originalString = "howtodoinjava.com";
//        String encryptedString = fileEncryptor.encrypt(originalString, secretKey) ;
//        String decryptedString = fileEncryptor.decrypt(encryptedString, secretKey) ;
//
//        System.out.println(originalString);
//        System.out.println(encryptedString);
//        System.out.println(decryptedString);
    }

}
